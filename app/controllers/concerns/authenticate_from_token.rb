# app/controllers/concerns/token_authentication.rb

module AuthenticateFromToken
  extend ActiveSupport::Concern

  # Please see https://gist.github.com/josevalim/fb706b1e933ef01e4fb6
  # before editing this file, the discussion is very interesting.

  included do
    private :authenticate_user_from_token!
    # This is our new function that comes before Devise's one
    before_action :authenticate_user_from_token!
    # This is Devise's authentication
    #before_action :authenticate_user!
    #skip_before_action :authenticate_user!
  end

  # For this example, we are simply using token authentication
  # via parameters. However, anyone could use Rails's token
  # authentication features to get the token from a header.
  #
  def authenticate_user_from_token!
    user_token = params[:auth_token] || request.headers["X-Auth-Token"]
    user = user_token.present? ? User.find_by(authentication_token: user_token) : nil
    if user
      sign_in(:user, user)
      # Notice we are passing store false, so the user is not
      # actually stored in the session and a token is needed
      # for every request. If you want the token to work as a
      # sign in token, you can simply remove store: false.
      # sign_in user, store: false
    else
      render json: { message: 'User not authenticated from auth token' }, status: :unprocessable_entity
    end
  end


  module ClassMethods
    # nop
  end
end