class Api::V1::RegistrationsController < Api::V1::BaseController

   def create
     user = User.new(user_params)
     if user.save
       render json: user, status: 200
       return
     else
       warden.custom_failure!
       render json: user.errors, status: 422
     end
   end

   private
   # Using a private method to encapsulate the permissible parameters is
   # just a good pattern since you'll be able to reuse the same permit
   # list between create and update. Also, you can specialize this method
   # with per-user checking of permissible attributes.
   def user_params
     params.require(:user).permit(:first_name, :last_name,:email, :username, :password)
   end

end
