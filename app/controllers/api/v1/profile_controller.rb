class Api::V1::ProfileController < Api::V1::BaseController

  include AuthenticateFromToken

  before_action :ensure_password_params_exist  ,only: [:reset_password]


  # if current_password present validate that and reset password
  def reset_password
    @user = current_user
    @user.password= params[:password]
    @user.password_confirmation = params[:password]
    if @user.save
      render json: {success: true}, status: 200
    else
      warden.custom_failure!
      render json: @user.errors, status: 422
    end
  end


  protected
  def ensure_password_params_exist
    error = nil
    error = "password can't be blank" if params[:password].blank?
    error = "current password not correct" if params[:current_password].present? and
                                         !current_user.valid_password?(params[:current_password])
    unless error.nil?
      render json: {success: false, error: error }, status: 422 #:unauthorized
    end
  end

end
