class Api::V1::CollegesController < Api::V1::BaseController
  #before_action :doorkeeper_authorize! # Require access token for all actions

  #before_action -> { doorkeeper_authorize! :public }, only: :index

  include AuthenticateFromToken

  #skip_before_action :authenticate_user_from_token!
  #skip_before_action :authenticate_user!

  # respond_to :json
  def index
    @colleges = College.all
  end

end
