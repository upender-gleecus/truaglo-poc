class Api::V1::SessionsController < Api::V1::BaseController

  include AuthenticateFromToken

  # Some of the actions we don't need to authenticate user example SignIn etc..
  skip_before_action :authenticate_user_from_token!, only: :create
  #skip_before_action :authenticate_user!, only: :create

  before_action :ensure_params_exist  ,only: [:create]

  def create
    resource = User.find_for_database_authentication(email: params[:email])
    return invalid_login_attempt unless resource
    if resource.valid_password?(params[:password])
      # sign_in(:user, resource)
      assign_resource_to_token(resource)
      render json: resource, status: 200
      return
    end
    invalid_login_attempt
  end

  def destroy
     ## ===> Revoke Token so that the access_token will not valid any long more
     # and user need to again ask for fresh access_token
     doorkeeper_token.update_column(:revoked_at, Time.now) if doorkeeper_token
     #doorkeeper_token.update_attribute(:revoked_at, Time.now) if doorkeeper_token
     reset_session
     render json: {success: true} , status: 200
  end

  protected
  def ensure_params_exist
    if params[:email].blank? or params[:password].blank?
      render json: {success: false, message: "Email or Password can't be blank" }, status: 422 #:unauthorized
    end
  end

  def invalid_login_attempt
    render json: {success: false, message: 'Error with your email or password' }, status: 401
  end



end
