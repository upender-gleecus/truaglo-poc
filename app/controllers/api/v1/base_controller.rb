module Api
  module V1
    class BaseController < ApplicationController

      before_action :doorkeeper_authorize! # Require access token for all actions

      # => To fix
      # can't verify CSRF token authenticity.
      # ActionController::InvalidAuthenticityToken (ActionController::InvalidAuthenticityToken)
      skip_before_action :verify_authenticity_token


      def doorkeeper_unauthorized_render_options(error: nil)
        #print_unauthorize_log
        {:json => { status: 401 , message: 'Application authentication failed.', error: error }}
      end

      # assign doorkeeper token to user when loggedIn
      def assign_resource_to_token(user, remember_me=false)
        doorkeeper_token.update_columns(resource_owner_id: user.id, expires_in: remember_me ? 999.days.to_i : 30.days.to_i)
      end

      private
      def print_unauthorize_log
        #reset_session
        Rails.logger.info "**************** Api Unauthorized Access Log ****************"
        Rails.logger.info "access_token: #{params[:access_token]}"
        Rails.logger.info "auth_token: #{params[:auth_token]}"
        Rails.logger.info "************** End of Api Unauthorized Access Log ***********"
      end

    end
  end
end