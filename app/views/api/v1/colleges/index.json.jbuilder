json.colleges @colleges do |college|
  json.id college.id
  json.name college.name
end