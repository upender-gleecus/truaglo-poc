class User < ApplicationRecord

  include TokenAuthenticable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :confirmable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  has_many :access_grants, class_name: "Doorkeeper::AccessGrant",
           foreign_key: :resource_owner_id,
           dependent: :delete_all # or :destroy if you need callbacks

  has_many :access_tokens, class_name: "Doorkeeper::AccessToken",
           foreign_key: :resource_owner_id,
           dependent: :delete_all # or :destroy if you need callbacks


  # ===> CallBacks



  def self.authenticate!(email, password)
    user = User.where(email: email).first
    return (user.valid_password?(password) ? user : nil) unless user.nil?
    nil
  end



end
