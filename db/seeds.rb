# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

SuperAdmin.destroy_all

SuperAdmin.create(
    email: 'upender@gleecus.com',
    password: '123456',
    confirmed_at: DateTime.now
)
User.destroy_all

User.create(email: 'user1@gleecus.com', username: 'user1',password: '123456',confirmed_at: DateTime.now)
User.create(email: 'user2@gleecus.com', username: 'user2',password: '123456',confirmed_at: DateTime.now)
User.create(email: 'user3@gleecus.com', username: 'user3',password: '123456',confirmed_at: DateTime.now)
User.create(email: 'user4@gleecus.com', username: 'user4',password: '123456',confirmed_at: DateTime.now)

College.create(name: 'college1' , city: 'city1')
College.create(name: 'college2' , city: 'city2')
College.create(name: 'college3' , city: 'city3')