Rails.application.routes.draw do
  use_doorkeeper
  root to: "pages#index"
  devise_for :users
  devise_for :super_admins
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api   do
    #version 1 --------------------------------
    namespace :v1 do
      get 'colleges', to: 'colleges#index'
     # devise_for :users,controllers: []#, defaults: { format: :json } , as: :users
      devise_scope :user do
        post   '/users/sign_in'  => 'sessions#create' # /api/v1/users/sign_in
        delete '/users/sign_out' => 'sessions#destroy' # /api/v1/users/sign_out
        # Registration
        post '/users/sign_up' => 'registrations#create' # /api/v1/users/sign_up
        # Change Password
        patch '/users/reset_password' => 'profile#reset_password' # /api/v1/users/reset_password
      end
    end
  end

  scope 'api' do
    use_doorkeeper :scope=>'clients' do
      #===> urls
      #POST api/v1/clients/token etc..
      #-----------

      # it accepts :authorizations, :tokens, :applications and :authorized_applications
      #controllers :applications => 'admin/api_provider'
    end
  end

end
